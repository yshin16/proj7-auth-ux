"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

# Import Flask-Related Features
import flask
from flask import redirect, url_for, request, flash
# Import App-Related Features
import arrow
import config
import acp_times  # Brevet time calculations
import logging
import sys
import os

# Import Database
from pymongo import MongoClient
# Import API
from flask_restful import Resource, Api
# Import Login Features
from flask_login import (LoginManager, current_user, login_required,
                            login_user, logout_user, UserMixin,
                            confirm_login, fresh_login_required)
# WTF Forms
from flask_wtf import FlaskForm
from wtforms import Form, BooleanField, StringField, validators, PasswordField
from forms import LoginForm, RegistrationForm


###
# Globals
###

class User(UserMixin):
    def __init__(self, username, password, active=True):
        self.username = username
        self.password = password
        self.active = active

    def is_active(self):
        return self.active

USERS = {
    1: User(u"A", "a"),
    2: User(u"B", "b"),
}

#USER_NAMES = dict((u.name, u) for u in USERS.itervalues())


app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
# Database
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb
# API
api = Api(app)
# login
login_manager = LoginManager()
login_manager.setup_app(app)

# step 2 in slides
@login_manager.user_loader
def load_user(username):
    return USERS.get(username)

@app.route("/login", methods=["GET", "POST"])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        login_user(user)
        flash("logged in")
        next = request.args.get("next")
        if not is_safe_url("next"):
            return flask.abort(400)
        return redirect(url_for("index"))
    return render_template("login.html", form=form)


###
# Pages
###

@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegistrationForm()
    #if request.method == 'POST':
    if form.validate_on_submit():
        user = User(username=form.username.data, password=form.password.data)
        return redirect(url_for('register'))
    return render_template('register.html', form=form)
'''
    user = User()
    user.username = form.username.data
    user.password = form.password.data
    user.save()
    redirect(url_for('register'))

    return render_response('register.html', form=form)
'''




class Time_Data(Resource):
    def get(self):
        _oitems = db.tododb.find({"name":"open_time"})
        oitems = []
        for item in _oitems:
            item['_id'] = str(item['_id'])
            oitems.append(item)
        _citems = db.tododb.find({"name":"close_time"})
        citems = []
        for item in _citems:
            item['_id'] = str(item['_id'])
            citems.append(item)
        return {
            'open_time': oitems,
            'close_time': citems
        }

api.add_resource(Time_Data, '/List_All')


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")

    km = request.args.get('km', 999, type=float)
    tdistance = request.args.get('tdistance', type=str)
    begin_date = request.args.get('begin_date', type=str)
    begin_time = request.args.get('begin_time', type=str)

    start_date = begin_date + " " + begin_time
    date = arrow.get(start_date, "YYYY-MM-DD HH:mm").isoformat()

    open_time = acp_times.open_time(km, tdistance, date)
    close_time = acp_times.close_time(km, tdistance, date)

    result = {"open": open_time, "close": close_time}


    return flask.jsonify(result=result)

# Function for submiting data
@app.route('/new', methods=["POST"])
def new():

    open_t = {
        'name': 'open_time',
        'description': request.form.get("open")

        }
    db.tododb.insert_one(open_t)

    close_t = {
        'name': 'close_time',
        'description': request.form.get("close")
        }
    db.tododb.insert_one(close_t)

    return redirect(url_for('index'))

# This function is suppose to render display.html with database info
@app.route("/display", methods=["POST"])
def display():
    _oitems = db.tododb.find({"name":"open_time"})
    oitems = [item for item in _oitems]

    if len(oitems) == 0:
        return flask.render_template("empty.html")

    _citems = db.tododb.find({"name":"close_time"})
    citems = [item for item in _citems]

    return flask.render_template('display.html', oitems=oitems, citems=citems)




#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")

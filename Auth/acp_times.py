"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """

    speed_dict = {
        "200" : 34,
        "300" : 32,
        "400" : 30,
        "600" : 28,
        "1000": 26
        }

    max_speed = speed_dict[brevet_dist_km]
    add_hour = round((control_dist_km / max_speed),2)

    hourcalc = str(add_hour).split(".")
    min = round((float(hourcalc[1]) * 0.01 * 60), 0)
    min = int(min)
    hr = int(hourcalc[0])

    currentdate = arrow.get(brevet_start_time)
    currentdate = currentdate.shift(hours=+hr, minutes=+min)

    return currentdate.isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    if isinstance(brevet_dist_km, str) == False:
        return arrow.now().isoformat()

    speed_dict = {
        "200" : 15,
        "300" : 15,
        "400" : 15,
        "600" : 11.428,
        "1000": 13.333
        }

    min_speed = speed_dict[brevet_dist_km]
    add_hour = round((control_dist_km / min_speed),2)

    hourcalc = str(add_hour).split(".")
    min = round((float(hourcalc[1]) * 0.01 * 60), 0)
    min = int(min)
    hr = int(hourcalc[0])


    currentdate = arrow.get(brevet_start_time)
    currentdate = currentdate.shift(hours=+hr, minutes=+min)

    return currentdate.isoformat()
